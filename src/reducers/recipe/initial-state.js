import { Record } from "immutable";

export default new Record({
	isLoading: false,
	error: null,
	category: "Beef",
	data: [
		// {
		//	"uuid": "0"
		// 	"cookTime": "5min",
		// 	"datePublished": "2009-07-06",
		// 	"description": "From the Big Sur Bakery cookbook, a ...",
		// 	"image": "http://www.101cookbooks.com/mt-static/images/food/big_sur_bakery_hide_bread.jpg",
		// 	"ingredients": [
		// 		{
		// 			"quantity": "5",
		// 			"unit": "cup",
		// 			"name": "flour",
		// 			"input": "all-purpose flour, plus extra flour for dusting"
		// 		},
		// 	...
		// 	],
		// 	"name": "Big Sur Bakery Hide Bread Recipe",
		// 	"prepTime": "5min",
		// 	"recipeYield": 1,
		// 	"url": "http://www.101cookbooks.com/archives/big-sur-bakery-hide-bread-recipe.html",
		// 	"instructions": "Roll the pastry to a 3mm-thick ...",
		// 	"Vegetarian": true,
		// 	"Japanese": true
		// }
	]
})();
