import { Record } from "immutable";

export default new Record({
	isLoading: false,
	error: null,
	data: [
		// {
		//	id: 000,
		//  createdAt: ...,
		// 	ref: 0000,
		// 	name: "",
		// 	note: "",
		// 	ingredients: [
		// 		{
		//      id: 0,
		// 			quantity: "1",
		// 			ingredient: "egg",
		// 			checked: false
		// 		}
		// 	]
		// }
	],
	form: new Record({
		isLoading: false,
		error: null,
		id: "",
		name: "",
		note: "",
		ingredients: [
			// {
			//  id: 0,
			// 	quantity: "1",
			// 	ingredient: "egg",
			// 	checked: false
			// }
		],
		currentId: -1
	})()
})();
