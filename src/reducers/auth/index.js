import { Types } from "../../actions/auth";
import initialState from "./initial-state";

export default (state = initialState, action) => {
	switch (action.type) {
		case Types.changeFormField:
			return state.setIn(["form", action.payload.key], action.payload.value);

		case Types.changeField:
			return state.set(action.payload.key, action.payload.value);

		case Types.resetForm:
			return state.set("form", initialState.form);

		case Types.setUserData:
			return state.set("user", action.payload.user);

		case Types.toggleCheck:
			return state.setIn(["form", "isChecked"], !state.form.isChecked);
		case Types.checkEmail:
			return state.setIn(["form", "emailError"], action.payload.error);
		case Types.checkPassword:
			return state.setIn(["form", "passwordError"], action.payload.error);
		case Types.checkConfirm:
			return state.setIn(["form", "confirmError"], action.payload.error);

		case Types.loginRequest:
			return state.set("isFetching", true).set("error", null);
		case Types.loginSuccess:
			return state
				.set("isFetching", false)
				.set("error", null)
				.set("logoutState", false);
		case Types.loginFailure:
			return state.set("isFetching", false).set("error", action.payload.error);

		case Types.logoutRequest:
			return state.set("isFetching", true).set("error", null);
		case Types.logoutSuccess:
			return state
				.set("isFetching", false)
				.set("error", null)
				.set("logoutState", true);
		case Types.logoutFailure:
			return state.set("isFetching", false).set("error", action.payload.error);

		case Types.registerRequest:
			return state.set("isFetching", true).set("error", null);
		case Types.registerSuccess:
			return state
				.set("isFetching", false)
				.set("error", null)
				.set("logoutState", false);
		case Types.registerFailure:
			return state.set("isFetching", false).set("error", action.payload.error);

		case Types.pushUserRequest:
			return state.set("isFetching", true).set("error", null);
		case Types.pushUserSuccess:
			return state
				.set("isFetching", false)
				.set("error", null)
				.set("user", action.payload.dataUser);
		case Types.pushUserFailure:
			return state
				.set("isFetching", false)
				.set("error", action.payload.error)
				// .set("user", null);

		case Types.getUserRequest:
			return state
				.set("isFetching", true)
				.set("error", null)
				// .set("user", null);
		case Types.getUserSuccess:
			return state
				.set("isFetching", false)
				.set("error", null)
				.set("user", action.payload.user);
		case Types.getUserFailure:
			return state
				.set("isFetching", false)
				.set("error", action.payload.error)
				// .set("user", null);
		default:
			return state;
	}
};
