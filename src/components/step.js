import React, { PureComponent } from "react";
import { View } from "react-native";
import { ScaledSheet } from "react-native-size-matters";
import { Button } from "react-native-elements";
import { Font, Color } from "../common";

export default class Step extends PureComponent {
  state = {};
  render() {
    const {
      children,
      onChangeValue,
      values,
      lockNextStep,
      currentIndex,
      prevStep,
      isLast,
      onSubmit,
      nextStep,
      nextTitle
    } = this.props;
    return (
      <View style={styles.container}>
        {children({
          onChangeValue: onChangeValue,
          values: values
        })}
        <View style={styles.btnWrapper}>
          {currentIndex !== 0 && (
            <Button
              type={"clear"}
              title={"Quay lại"}
              titleStyle={styles.btnTitle}
              onPress={prevStep}
              containerStyle={styles.btnBack}
            />
          )}
          <Button
            disabled={lockNextStep}
            title={nextTitle}
            titleStyle={styles.btnTitle}
            buttonStyle={styles.btnStyle}
            containerStyle={styles.btnContainer}
            onPress={isLast ? onSubmit : nextStep}
          />
        </View>
      </View>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1
  },
  btnWrapper: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: "12@vs"
  },
  btnTitle: {
    fontSize: Font.Size.header,
    fontFamily: Font.Family.OpenSansLight,
    color: "#fafafa"
  },
  btnBack: {
    marginRight: "16@ms"
  },
  btnStyle: {
    backgroundColor: Color.turquoise
  },
  btnContainer: {
    flex: 1
  }
});
