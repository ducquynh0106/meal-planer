import React, { Component } from "react";
import {
	View,
	Text,
	Image,
	TouchableOpacity,
	Platform,
	Dimensions,
	StyleSheet,
	TouchableHighlight
} from "react-native";
import PropTypes from "prop-types";
import { ParallaxImage } from "react-native-snap-carousel";
import { Color } from "../common";
import LinearGradient from "react-native-linear-gradient";
import { Icon } from "react-native-elements";

const IS_IOS = Platform.OS === "ios";
const { width: viewportWidth, height: viewportHeight } = Dimensions.get(
	"window"
);

function wp(percentage) {
	const value = (percentage * viewportWidth) / 100;
	return Math.round(value);
}

const slideHeight = viewportHeight * 0.66;
const slideWidth = wp(85);
const itemHorizontalMargin = wp(0.7);

export const sliderWidth = viewportWidth;
export const itemWidth = slideWidth + itemHorizontalMargin * 2;

const entryBorderRadius = 8;

export default class SliderEntry extends Component {
	static propTypes = {
		data: PropTypes.object.isRequired,
		parallax: PropTypes.bool,
		parallaxProps: PropTypes.object,
		onSave: PropTypes.func,
		onPress: PropTypes.func,
		uid: PropTypes.string
	};

	state = {};

	get image() {
		const {
			data: { image },
			parallax,
			parallaxProps
		} = this.props;
		return parallax ? (
			<ParallaxImage
				source={{ uri: image }}
				containerStyle={styles.imageContainer}
				style={styles.image}
				parallaxFactor={0.35}
				showSpinner={true}
				spinnerColor={"rgba(255, 255, 255, 0.4)"}
				{...parallaxProps}
			/>
		) : (
			<Image source={{ uri: strMealThumb }} style={styles.image} />
		);
	}

	handleSave = () => {
		this.setState({ liked: !this.state.liked });
		this.props.onSave();
	};

	render() {
		const {
			data: { name, category },
			onSave,
			onPress,
			uid
		} = this.props;

		const uppercaseTitle = name ? (
			<Text style={styles.title} numberOfLines={2}>
				{name.toUpperCase()}
			</Text>
		) : (
			false
		);

		return (
			<TouchableHighlight underlayColor={"#eee"} onPress={onPress}>
				<View style={styles.slideInnerContainer}>
					<View style={styles.imageContainer}>{this.image}</View>
					<LinearGradient
						colors={["rgba(255,255,255,.1)", "#000"]}
						style={styles.gradient}
					/>

					<View style={styles.textContainer}>
						<View style={styles.row}>
							<View style={styles.left}>
								{uppercaseTitle}
								<Text style={styles.subtitle} numberOfLines={2}>
									{category}
								</Text>
							</View>

							<TouchableOpacity style={styles.save} onPress={this.handleSave}>
								<Icon
									name={this.props.data[uid] ? "heart" : "hearto"}
									type={"antdesign"}
									size={24}
									color={this.props.data[uid] ? Color.red : "#fafafa"}
								/>
							</TouchableOpacity>
						</View>
					</View>
				</View>
			</TouchableHighlight>
		);
	}
}

const styles = StyleSheet.create({
	slideInnerContainer: {
		width: itemWidth,
		height: slideHeight
	},
	mask: {
		...StyleSheet.absoluteFillObject,
		backgroundColor: "rgba(0,0,0,.2)",
		borderRadius: entryBorderRadius
	},
	gradient: {
		...StyleSheet.absoluteFillObject,
		borderRadius: entryBorderRadius
	},
	imageContainer: {
		flex: 1,
		marginBottom: IS_IOS ? 0 : -1, // Prevent a random Android rendering issue
		backgroundColor: Color.primaryDark,
		borderTopLeftRadius: entryBorderRadius,
		borderTopRightRadius: entryBorderRadius
	},
	image: {
		...StyleSheet.absoluteFillObject,
		resizeMode: "cover",
		borderRadius: IS_IOS ? entryBorderRadius : 0,
		borderTopLeftRadius: entryBorderRadius,
		borderTopRightRadius: entryBorderRadius
	},
	// image's border radius is buggy on iOS; let's hack it!
	textContainer: {
		justifyContent: "center",
		paddingTop: 20 - entryBorderRadius,
		paddingBottom: 20,
		paddingHorizontal: 16,
		backgroundColor: Color.primaryDark,
		borderBottomLeftRadius: entryBorderRadius,
		borderBottomRightRadius: entryBorderRadius
	},
	title: {
		color: "white",
		fontSize: 13,
		fontWeight: "bold",
		letterSpacing: 0.5
	},
	subtitle: {
		color: "rgba(255, 255, 255, 0.7)",
		marginTop: 6,
		fontSize: 12,
		fontStyle: "italic"
	},
	row: {
		flexDirection: "row",
		alignItems: "center"
	},
	left: {
		flex: 1
	},
	right: {},
	save: {
		marginLeft: 16
	}
});
