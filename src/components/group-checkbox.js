import React, { Component } from "react";
import PropTypes from "prop-types";
import {
	View,
	Text,
	FlatList,
	TouchableOpacity,
	StyleSheet
} from "react-native";
import { Icon } from "react-native-elements";
import { Color, Font } from "../common";
import { verticalScale, moderateScale } from "react-native-size-matters";

export default class GroupCheckbox extends Component {
	constructor(props) {
		super(props);
		this.state = {
			options: this.validate(this.props.options)
		};
	}
	validate(data) {
		data.map((item, index) => {
			item.key = index.toString();
			item.selected = false;
		});
		data[0].selected = true;
		return data;
	}
	onPress = index => {
		const options = this.state.options;
		const active = options.find(e => e.selected === true);
		active ? (active.selected = false) : null;
		options[index].selected = true;
		this.setState({ options });
		this.props.onCheck(index);
	};
	renderItem = ({ item, index }) => {
		return (
			<TouchableOpacity
				onPress={() => this.onPress(index)}
				style={styles.option}
			>
				<Icon
					name={item.selected ? "check-circle" : "circle"}
					type={"feather"}
					size={24}
					color={item.selected ? Color.lightBlue : "rgba(255,255,255,.1)"}
				/>
				<Text style={styles.text}>{item.title}</Text>
			</TouchableOpacity>
		);
	};
	render() {
		const { containerStyle } = this.props;
		return (
			<View style={StyleSheet.flatten([styles.container, containerStyle])}>
				<FlatList
					data={this.state.options}
					extraData={this.state}
					keyExtractor={item => item.key}
					renderItem={this.renderItem}
					showsVerticalScrollIndicator={false}
				/>
			</View>
		);
	}
}

GroupCheckbox.propTypes = {
	onCheck: PropTypes.func,
	options: PropTypes.array,
	containerStyle: PropTypes.object
};

GroupCheckbox.defaultProps = {
	onCheck: () => {},
	options: [{ title: "test" }, { title: "test test" }],
	containerStyle: {}
};

const styles = StyleSheet.create({
	option: {
		flexDirection: "row",
		alignItems: "center",
		paddingVertical: verticalScale(8)
	},
	container: {
		paddingVertical: verticalScale(12)
	},
	text: {
		fontSize: Font.Size.body1,
		color: "#fff",
		marginLeft: moderateScale(16)
	}
});
