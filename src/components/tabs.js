import React, { Component } from "react";
import PropTypes from "prop-types";
import { Text, TouchableOpacity, FlatList } from "react-native";

const selectedStyles = {
	color: "black",
	fontSize: 30,
	fontWeight: "500",
	marginRight: 16,
	textAlignVertical: "center"
};
const unselectedStyles = {
	color: "#ccc",
	fontSize: 20,
	fontWeight: "normal",
	marginRight: 16,
	textAlignVertical: "center"
};

export default class Tabs extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tabs: this.validate(this.props.tabs)
		};
	}
	static propTypes = {
		tabs: PropTypes.array,
		onTabPress: PropTypes.func
	};
	static defaultProps = {
		onTabPress: () => {}
	};
	validate(data) {
		data.map((item, index) => {
			item.key = index.toString();
			item.selected = false;
		});
		data[0].selected = true;
		return data;
	}
	onPress = key => {
		const tabs = this.state.tabs;
		const active = tabs.find(e => e.selected === true);
		active ? (active.selected = false) : null;
		tabs.find(e => e.key === key).selected = true;
		this.setState({ tabs });
		this.flatList.scrollToIndex({ index: key, viewPosition: 0.5 });
		this.props.onTabPress(this.state.tabs[key].strCategory);
	};
	render() {
		return (
			<FlatList
				ref={ref => (this.flatList = ref)}
				horizontal
				showsHorizontalScrollIndicator={false}
				keyExtractor={item => item.key}
				data={this.state.tabs}
				extraData={this.state}
				contentContainerStyle={{ alignItems: "center" }}
				renderItem={({ item, index }) => (
					<TouchableOpacity onPress={() => this.onPress(item.key)}>
						<Text
							style={[
								item.selected ? selectedStyles : unselectedStyles,
								index === 0 ? { paddingLeft: 16 } : null
							]}
						>
							{item.strCategory}
						</Text>
					</TouchableOpacity>
				)}
			/>
		);
	}
}
