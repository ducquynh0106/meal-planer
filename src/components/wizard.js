import React, { PureComponent } from "react";
import { View } from "react-native";
import { ScaledSheet } from "react-native-size-matters";
import Step from "./step";

export default class Wizard extends PureComponent {
  static Step = Step;
  state = {
    index: 0,
    values: {
      ...this.props.initialValues
    }
  };
  _prevStep = () => {
    if (this.state.index !== 0) {
      this.setState(prevState => ({
        index: prevState.index - 1
      }));
    }
  };
  _nextStep = () => {
    if (this.state.index !== this.props.children.length - 1) {
      this.setState(prevState => ({
        index: prevState.index + 1
      }));
    }
  };
  _onChangeValue = (name, value) => {
    this.setState(prevState => ({
      values: {
        ...prevState.values,
        [name]: value
      }
    }));
  };
  _onSubmit = () => {
    this.props.onSubmit(this.state.values);
  };
  render() {
    console.log("this.state.values :", this.state.values);
    const { index, values } = this.state;
    const { children } = this.props;
    return (
      <View style={styles.container}>
        {React.Children.map(children, (el, i) => {
          if (i === index) {
            return React.cloneElement(el, {
              currentIndex: index,
              nextTitle: (values.titles && values.titles[index]) || "Tiếp theo",
              nextStep: this._nextStep,
              lockNextStep: this.props.lockNextStep,
              prevStep: this._prevStep,
              isLast: index === children.length - 1,
              onChangeValue: this._onChangeValue,
              values: values,
              onSubmit: this._onSubmit
            });
          }
        })}
      </View>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1
  }
});
