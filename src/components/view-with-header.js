import React, { Component } from "react";
import CustomView from "./custom-view";
import { ScaledSheet } from "react-native-size-matters";
import { Animated, StyleSheet } from "react-native";
import { Color } from "../common";

export default class ViewWithHeader extends Component {
	constructor(props) {
		super(props);

		const scrollAnim = new Animated.Value(0);
		const offsetAnim = new Animated.Value(0);

		this.state = {
			scrollAnim,
			offsetAnim,
			clampedScroll: Animated.diffClamp(
				Animated.add(
					scrollAnim.interpolate({
						inputRange: [0, 1],
						outputRange: [0, 1],
						extrapolateLeft: "clamp"
					}),
					offsetAnim
				),
				0,
				56
			)
		};
	}
	_clampedScrollValue = 0;
	_offsetValue = 0;
	_scrollValue = 0;
	componentDidMount() {
		this.state.scrollAnim.addListener(({ value }) => {
			const diff = value - this._scrollValue;
			this._scrollValue = value;
			this._clampedScrollValue = Math.min(
				Math.max(this._clampedScrollValue + diff, 0),
				56
			);
		});
		this.state.offsetAnim.addListener(({ value }) => {
			this._offsetValue = value;
		});
	}

	componentWillUnmount() {
		this.state.scrollAnim.removeAllListeners();
		this.state.offsetAnim.removeAllListeners();
	}
	_onScrollEndDrag = () => {
		this._scrollEndTimer = setTimeout(this._onMomentumScrollEnd, 250);
	};

	_onMomentumScrollBegin = () => {
		clearTimeout(this._scrollEndTimer);
	};

	_onMomentumScrollEnd = () => {
		const toValue =
			this._scrollValue > 56 && this._clampedScrollValue > 56 / 2
				? this._offsetValue + 56
				: this._offsetValue - 56;

		Animated.timing(this.state.offsetAnim, {
			toValue,
			duration: 350,
			useNativeDriver: true
		}).start();
	};

	render() {
		const { children, header, ...rest } = this.props;
		const { clampedScroll } = this.state;
		const headerTranslate = clampedScroll.interpolate({
			inputRange: [0, 56],
			outputRange: [0, -56],
			extrapolate: "clamp"
		});
		const headerOpacity = clampedScroll.interpolate({
			inputRange: [0, 56],
			outputRange: [1, 0],
			extrapolate: "clamp"
		});
		const stickyOpacity = this.state.scrollAnim.interpolate({
			inputRange: [0, 56],
			outputRange: [0, 1],
			extrapolate: "clamp"
		});
		const listView = children ? (
			<Animated.ScrollView
				onMomentumScrollBegin={this._onMomentumScrollBegin}
				onMomentumScrollEnd={this._onMomentumScrollEnd}
				onScrollEndDrag={this._onScrollEndDrag}
				onScroll={Animated.event(
					[{ nativeEvent: { contentOffset: { y: this.state.scrollAnim } } }],
					{ useNativeDriver: true }
				)}
				contentContainerStyle={styles.content}
				{...rest}
			>
				{children}
			</Animated.ScrollView>
		) : (
			<Animated.FlatList
				onMomentumScrollBegin={this._onMomentumScrollBegin}
				onMomentumScrollEnd={this._onMomentumScrollEnd}
				onScrollEndDrag={this._onScrollEndDrag}
				onScroll={Animated.event(
					[{ nativeEvent: { contentOffset: { y: this.state.scrollAnim } } }],
					{ useNativeDriver: true }
				)}
				contentContainerStyle={styles.content}
				{...rest}
			/>
		);
		return (
			<CustomView transparent>
				{listView}
				<Animated.View
					style={[
						styles.header,
						{
							opacity: headerOpacity,
							transform: [{ translateY: headerTranslate }]
						}
					]}
				>
					<Animated.View style={[styles.sticky, { opacity: stickyOpacity }]} />
					{header}
				</Animated.View>
			</CustomView>
		);
	}
}

const headerHeight = 56;

const styles = ScaledSheet.create({
	content: {
		padding: "8@ms",
		paddingTop: headerHeight
	},
	header: {
		height: headerHeight,
		position: "absolute",
		top: 0,
		left: 0,
		right: 0
	},
	sticky: {
		...StyleSheet.absoluteFillObject,
		backgroundColor: "#fff",
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: "#ccc"
	}
});
