import { createSelector } from "reselect";

export const getEntireCategoryState = state => state.category;

export const getCategoryField = (state, field) => state.category[field];
