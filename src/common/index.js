import font from './font';
import color from './color';

export const Font = font;
export const Color = color;
