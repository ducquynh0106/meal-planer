import { ScaledSheet } from "react-native-size-matters";
import { Color, Font } from "../../common";

export const styles = ScaledSheet.create({
	container: {
		paddingTop: 56,
		backgroundColor: "#fff",
		marginHorizontal: 8,
		overflow: "hidden"
	},
	modalContent: { width: "280@ms0.2" },
	add: {
		color: Color.lightGreen,
		fontFamily: Font.Family.OpenSansLightItalic,
		fontSize: 18
	},
	modalButton: {
		fontSize: 16,
		fontFamily: Font.Family.OpenSansLight,
		color: Color.lightGreen
	},
	modalTitle: {
		fontSize: 16
	},
	leftContent: {
		flex: 1,
		backgroundColor: Color.red,
		textAlign: "right",
		textAlignVertical: "center",
		paddingRight: "16@ms",
		color: "#fafafa",
		borderRadius: 8
	},
	itemContainer: {
		backgroundColor: "#f9f9f9",
		paddingHorizontal: "16@ms",
		paddingVertical: "12@vs",
		borderRadius: 4
	},
	row: {
		flexDirection: "row",
		textAlign: "center"
	},
	itemQuantity: {
		fontSize: 20,
		color: Color.orange
	},
	itemIngredient: {
		fontSize: 20,
		flex: 1,
		textAlign: "center"
	},
	listHeader: {
		paddingBottom: "8@vs"
	},
	btnTitle: {
		fontSize: 20,
		fontFamily: Font.Family.OpenSansRegular,
		letterSpacing: 1
	},
	btnStyle: {
		backgroundColor: Color.lightGreen
	},
	btnContainer: {
		margin: "16@ms"
	}
});
