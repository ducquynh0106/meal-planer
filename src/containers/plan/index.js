import React, { Component } from "react";
import {
  View,
  Text,
  TouchableHighlight,
  ScrollView,
  TouchableOpacity,
  Image
} from "react-native";
import { styles } from "./styles";
import CustomView from "../../components/custom-view";
import CalendarStrip from "react-native-slideable-calendar-strip";
import { connect } from "react-redux";
import { convertTimeToString } from "../../common/util";
import PlanAction from "../../actions/plan";
import { Icon, Button } from "react-native-elements";
import { Color, Font } from "../../common";
import { Router } from "../../navigation/route";
import Loader from "../../components/loader";
import Snackbar from "react-native-snackbar";
import _ from "lodash";

const keys = ["breakfast", "lunch", "dinner"];
const format = "yyyy-MM-dd";

class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDate: new Date(),
      editMode: false
    };
    this.tempForm = null;
  }

  handleGetPlans = () => {
    console.log("handle get plans");

    this.props
      .getPlans(this.props.user.uid)
      .then(() => {
        this.handleChange();
      })
      .catch(error => {
        Snackbar.show({
          title: "Something went wrong, please try later"
        });
        console.log("====================================");
        console.log("Plan-get plans failed", error);
        console.log("====================================");
      });
  };

  componentDidMount() {
    console.log("did mount", this.props.user);

    this.handleGetPlans();
  }

  onSave = () => {
    if (this.state.editMode) {
      this.toggleEdit();
    }

    this.props
      .setPlan()
      .then(() => {
        Snackbar.show({
          title: "Save plan successfully!"
        });
        this.handleGetPlans();
      })
      .catch(error => {
        Snackbar.show({
          title: "Something went wrong, please try later"
        });
        console.log("====================================");
        console.log("Save plan failed", error);
        console.log("====================================");
      });
  };

  handleChange = () => {
    console.log("handle change");

    const { data, setForm, resetForm, changeFormField, form } = this.props;
    const { selectedDate } = this.state;
    const dateString = convertTimeToString(selectedDate, format);
    const temp = data.filter(e => e.dateString === dateString);

    if (temp.length > 0) {
      setForm(temp[0]);
    } else {
      resetForm();
      changeFormField("dateString", dateString);
    }

    //get temp
    this.tempForm = form;
  };

  addRecipe = key => {
    if (!this.state.editMode) {
      this.toggleEdit();
    }

    this.props.navigation.navigate(Router.CreateCollection, {
      type: "select",
      key
    });
  };

  removeRecipe = (item, key) => {
    if (!this.state.editMode) {
      this.toggleEdit();
    }

    this.props.removeItem(item, key);
  };
  toDetails = item => {
    this.props.navigation.navigate(Router.RecipeDetails, { item });
  };
  toggleEdit = () => {
    this.setState({ editMode: !this.state.editMode });
  };

  onPressDate = selectedDate => {
    this.setState({ selectedDate, editMode: false }, () => {
      this.handleChange();
    });
  };

  getMarked = data => data.map(e => e.dateString);
  cancel = () => {
    // const { setForm, form } = this.props;
    // _.isEqual(form, this.tempForm) ? null : setForm(this.tempForm);
    // console.log("====================", _.isEqual(form, this.tempForm));
    this.handleGetPlans();

    this.toggleEdit();
  };

  addToList = () => {
    const {
      form: { breakfast, lunch, dinner }
    } = this.props;
    let ingredients = [];
    breakfast.map(e => {
      ingredients = this.union(ingredients, e.ingredients);
    });
    lunch.map(e => {
      ingredients = this.union(ingredients, e.ingredients);
    });
    dinner.map(e => {
      ingredients = this.union(ingredients, e.ingredients);
    });

    this.props.navigation.navigate(Router.AddToShop, { ingredients });
  };

  union = (arr1, arr2) => {
    console.log("test length", arr1.length, arr2.length);

    if (arr1.length === 0 && arr2.length !== 0) {
      return arr2;
    }
    if (arr1.length !== 0 && arr2.length === 0) {
      return arr1;
    }
    if (arr1.length === 0 && arr2.length === 0) {
      return [];
    }
    let temp = [...arr1];
    arr2.map(e => {
      let filter = arr1.filter(item => item.name === e.name);
      if (filter.length === 0) {
        temp.push(e);
      } else {
        let newItem = Object.assign({}, e);
        filter.map(obj => {
          newItem.quantity = (
            parseFloat(newItem.quantity) + parseFloat(obj.quantity)
          ).toString();
        });
        let index = temp.findIndex(item => item.name === e.name);
        temp.splice(index, 1, newItem);
      }
    });
    return temp;
  };

  renderSection = key => {
    const { form } = this.props;
    const data = form[key];
    // console.log("render section", key, data);
    return (
      <View style={styles.section} key={key}>
        <View style={[styles.row, styles.sectionHeader]}>
          <Text style={styles.headerTitle}>{key}</Text>
          <TouchableOpacity onPress={() => this.addRecipe(key)}>
            <Icon
              name={"plus"}
              type={"antdesign"}
              size={24}
              color={"#999"}
              containerStyle={styles.add}
            />
          </TouchableOpacity>
        </View>
        {data.length > 0 &&
          data.map(item => (
            <TouchableHighlight
              key={key + item.uuid}
              underlayColor={"#eee"}
              onPress={() => this.toDetails(item)}
            >
              <View style={styles.row}>
                <TouchableOpacity onPress={() => this.removeRecipe(item, key)}>
                  <Icon
                    name={"trash-o"}
                    type={"font-awesome"}
                    color={Color.red}
                    size={24}
                    containerStyle={styles.remove}
                  />
                </TouchableOpacity>
                <Image
                  resizeMode={"contain"}
                  resizeMethod={"resize"}
                  style={{
                    width: 100,
                    height: 100,
                    borderRadius: 12
                  }}
                  source={{ uri: item.image }}
                />
                <Text style={styles.name}>{item.name}</Text>
              </View>
            </TouchableHighlight>
          ))}
      </View>
    );
  };

  render() {
    const { data } = this.props;
    console.log("====================================");
    // data.map(e => {
    // 	console.log(e.dateString);
    // });
    // console.log(this.getMarked(data));
    console.log("====================================");

    return (
      <CustomView>
        <Loader visible={this.props.isLoading} />
        <CalendarStrip
          selectedDate={this.state.selectedDate}
          onPressDate={date => this.onPressDate(date)}
          markedDate={this.getMarked(data)}
        />
        {this.state.editMode && (
          <View style={[styles.row, styles.footer]}>
            <Button
              onPress={this.cancel}
              type={"outline"}
              title={"Cancel"}
              titleStyle={{
                color: Color.lightGreen,
                fontFamily: Font.Family.OpenSansLight
              }}
              buttonStyle={{ borderColor: Color.lightGreen }}
              containerStyle={{ flex: 1, margin: 8 }}
            />
            <Button
              onPress={this.onSave}
              title={"Save"}
              titleStyle={{
                fontFamily: Font.Family.OpenSansLight
              }}
              buttonStyle={{ backgroundColor: Color.lightGreen }}
              containerStyle={{ flex: 1, margin: 8 }}
            />
          </View>
        )}
        <ScrollView
          showsVerticalScrollIndicator={false}
          // contentContainerStyle={{ height: "100%" }}
        >
          <TouchableOpacity onPress={this.addToList}>
            <Text style={styles.addIng}>Add to shopping list</Text>
          </TouchableOpacity>
          {keys.map(e => this.renderSection(e))}
        </ScrollView>
      </CustomView>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.plan.isLoading,
  data: state.plan.data,
  form: state.plan.form,
  user: state.auth.user
});

const mapDispatchToProps = {
  removeItem: PlanAction.removeItem,
  getPlans: PlanAction.getPlans,
  setForm: PlanAction.setForm,
  resetForm: PlanAction.resetForm,
  changeFormField: PlanAction.changeFormField,
  setPlan: PlanAction.setPlan
};

export default connect(mapStateToProps, mapDispatchToProps)(index);
