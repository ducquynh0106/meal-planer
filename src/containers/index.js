import Test from "./test";

import Splash from "./splash";
import Intro from "./intro";
import Explore from "./explore";
import Forgot from "./forgot";
import Login from "./login";
import Plan from "./plan";
import Profile from "./profile";
import Register from "./register";
import Shop from "./shop";
import Search from "./search";
import CreateCollection from "./create-collection";
import RecipeDetails from "./recipe-details";
import AddShopList from "./add-shop-list";
import AddToShop from "./add-to-shop";
import ListDetail from "./list-detail";
import CookDetail from "./cook-detail";

export default {
	Test,

	Splash,
	Intro,
	Explore,
	Forgot,
	Login,
	Plan,
	Profile,
	Register,
	Shop,
	Search,
	CreateCollection,
	RecipeDetails,
	AddShopList,
	AddToShop,
	ListDetail,
	CookDetail
};
