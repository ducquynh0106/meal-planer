import { ScaledSheet } from "react-native-size-matters";
import { Font } from "../../common";

export const styles = ScaledSheet.create({
	container: { flex: 1, alignItems: "center" },
	logo: {
		width: "200@ms",
		height: "200@ms",
		alignSelf: "center"
	},
	form: {
		width: "300@ms",
		backgroundColor: "rgba(0,0,0,.1)",
		padding: "16@vs",
		borderRadius: "8@vs"
	},
	title: {
		fontFamily: Font.Family.OpenSansSemiBold,
		color: "#f4f4f4",
		fontSize: 20,
		letterSpacing: 2,
		textAlign: "center"
	},
	insWrapper: {
		marginVertical: "20@vs",
		flexDirection: "row",
		alignSelf: "center"
	},
	instruction: {
		color: "#f4f4f4",
		fontFamily: Font.Family.OpenSansLightItalic,
		marginLeft: "16@ms"
	},
	highlight: {
		fontFamily: Font.Family.OpenSansItalic,
		color: "#fff"
	},
	createTitle: {
		textTransform: "uppercase",
		fontFamily: Font.Family.OpenSansSemiBold,
		color: "#666"
	},
	createButton: {
		backgroundColor: "#f4f4f4",
		padding: "16@vs",
		borderRadius: "40@vs",
		marginVertical: "16@vs",
		width: "240@ms0.3"
	}
});
