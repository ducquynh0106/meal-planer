import React, { Component } from "react";
import ViewWithHeader from "../../components/view-with-header";
import CustomHeader from "../../components/custom-header";
import { connect } from "react-redux";
import { View, TextInput, Text, TouchableOpacity, Image } from "react-native";
import styles from "./styles";
import { titleStyle, headerTitleRight } from "../../common/global";
import AuthAction from "../../actions/auth";
import Loader from "../../components/loader";
import Snackbar from "react-native-snackbar";

const defaultIngredient = {
	name: "",
	input: "",
	quantity: ""
};

class index extends Component {
	static navigationOptions = {
		header: null
	};
	constructor(props) {
		super(props);
		const {
			navigation: { getParam },
			user: { cookBook }
		} = props;
		const index = getParam("index", 0);

		this.state = {
			index,
			ingredients: this.normalize(cookBook[index].ingredients)
		};
	}
	normalize = data => {
		data.map((e, i) => {
			e.id = i;
		});
		return data;
	};
	finalize = data => {
		data.map(e => {
			if (e.name === "") {
				let temp = e.input.trim().split(" ");
				e.name = temp[temp.length - 1];
			}
		});
		return data;
	};
	componentWillReceiveProps(nextProps) {
		const { isFetching, error } = nextProps;
		if (error) {
			Snackbar.show({
				title: "Something went wrong, please try later" + error
			});
			this.props.navigation.goBack();
		} else if (!isFetching && this.props.isFetching) {
			Snackbar.show({
				title: "Update recipe successfully"
			});
			this.props.navigation.goBack();
		}
	}
	changeIngredient = (value, index) => {
		this.setState(prev => {
			let temp = [...prev.ingredients];
			temp[index].input = value;
			return {
				...prev,
				ingredients: temp
			};
		});
	};
	changeQty = (value, index) => {
		this.setState(prev => {
			let temp = [...prev.ingredients];
			temp[index].quantity = value;
			return {
				...prev,
				ingredients: temp
			};
		});
	};
	removeItem = index => {
		let temp = [...this.state.ingredients];
		temp.splice(index, 1);
		this.setState({ ingredients: this.normalize(temp) });
	};
	renderItem = ({ item }) => {
		return (
			<View style={[styles.row, { marginBottom: 8 }]}>
				<View style={styles.textInput}>
					<TextInput
						defaultValue={item.quantity}
						multiline
						onChangeText={value => this.changeQty(value, item.id)}
					/>
				</View>
				<View style={styles.input}>
					<TextInput
						defaultValue={item.input}
						multiline
						style={styles.ingredient}
						onChangeText={value => this.changeIngredient(value, item.id)}
					/>
				</View>
				<TouchableOpacity onPress={() => this.removeItem(item.id)}>
					<Text style={styles.x}>X</Text>
				</TouchableOpacity>
			</View>
		);
	};
	insert = () => {
		let newItem = Object.assign({}, defaultIngredient);
		let temp = [...this.state.ingredients];
		temp.unshift(newItem);
		this.setState({ ingredients: this.normalize(temp) });
	};
	onSave = () => {
		const {
			user: { cookBook },
			updateUser
		} = this.props;
		const { index } = this.state;
		const recipe = Object.assign({}, cookBook[index]);
		recipe.ingredients = this.state.ingredients;
		let temp = [...cookBook];
		temp.splice(index, 1, recipe);
		updateUser(this.props.user, {
			cookBook: temp
		});
	};
	renderListHeader = () => {
		const {
			user: { cookBook }
		} = this.props;
		const { index } = this.state;
		const recipe = cookBook[index];
		return (
			<View style={styles.header}>
				<View style={styles.row}>
					<Image
						resizeMethod={"resize"}
						resizeMode={"contain"}
						style={{ width: 100, height: 100, borderRadius: 16 }}
						source={{ uri: recipe.image }}
					/>
					<View style={styles.inner}>
						<Text>{recipe.name}</Text>
					</View>
				</View>
				<Text style={titleStyle}>Ingredients:</Text>
				<TouchableOpacity onPress={this.insert}>
					<Text style={styles.textAdd}>+ New ingredient</Text>
				</TouchableOpacity>
				<Loader visible={this.props.isFetching} />
			</View>
		);
	};
	renderListFooter = () => {
		const {
			user: { cookBook }
		} = this.props;
		const { index } = this.state;
		const recipe = cookBook[index];
		return (
			<View>
				<Text style={titleStyle}>Descriptions</Text>
				<Text>{recipe.description}</Text>
				<Text style={titleStyle}>Instructions</Text>
				<Text>{recipe.instructions}</Text>
			</View>
		);
	};
	render() {
		const { ingredients } = this.state;

		return (
			<ViewWithHeader
				contentContainerStyle={styles.container}
				showsVerticalScrollIndicator={false}
				header={
					<CustomHeader
						navigation={this.props.navigation}
						title={"My Recipe Details"}
						headerRight={
							<TouchableOpacity onPress={this.onSave}>
								<Text style={headerTitleRight}>Save</Text>
							</TouchableOpacity>
						}
					/>
				}
				data={ingredients}
				extraData={ingredients}
				keyExtractor={item => item.id.toString()}
				renderItem={this.renderItem}
				ListHeaderComponent={this.renderListHeader}
				ListFooterComponent={this.renderListFooter}
			/>
		);
	}
}

const mapStateToProps = state => ({
	user: state.auth.user,
	isFetching: state.auth.isFetching,
	error: state.auth.error
});

const mapDispatchToProps = {
	updateUser: AuthAction.updateUser
};

export default connect(mapStateToProps, mapDispatchToProps)(index);
