import { ScaledSheet } from "react-native-size-matters";
import { Font, Color } from "../../common";

export default ScaledSheet.create({
	container: { padding: "8@ms", paddingTop: 56 },
	header: {
		marginVertical: "16@vs"
	},
	row: {
		flexDirection: "row",
		alignItems: "center"
	},
	inner: {
		flex: 1,
		paddingLeft: "8@ms"
	},
	input: {
		flex: 3,
		paddingHorizontal: "8@ms"
	},
	textInput: {
		flex: 1
	},
	x: {
		padding: 8,
		fontFamily: Font.Family.OpenSansBold
	},
	textAdd: {
		color: Color.green,
		alignSelf: "flex-end"
	}
});
