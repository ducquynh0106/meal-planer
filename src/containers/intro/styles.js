import { ScaledSheet } from "react-native-size-matters";
import { Dimensions } from "react-native";
import { Font, Color } from "../../common";

const { height } = Dimensions.get("window");

export const styles = ScaledSheet.create({
	container: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center"
	},
	inner: { backgroundColor: "rgba(0,0,0,.1)" },
	backgroundVideo: {
		height,
		position: "absolute",
		left: 0,
		right: 0,
		bottom: 0,
		top: 0,
		alignItems: "stretch"
	},
	wrapper: {
		width: "320@ms",
		height: "600@vs",
		justifyContent: "space-around"
	},
	logo: {
		width: "100@ms",
		height: "100@ms",
		maxWidth: "100@ms",
		alignSelf: "center"
	},
	title: {
		color: "#f4f4f4",
		fontSize: 40,
		textAlign: "center",
		textTransform: "uppercase",
		fontFamily: Font.Family.OpenSansSemiBold,
		letterSpacing: 3
	},
	description: {
		color: "#f4f4f4",
		textAlign: "center",
		letterSpacing: 4,
		textTransform: "uppercase",
		fontFamily: Font.Family.OpenSansLightItalic
	},
	buttonWrapper: {
		width: "240@ms0.3",
		alignSelf: "center"
	},
	createTitle: {
		textTransform: "uppercase",
		fontFamily: Font.Family.OpenSansSemiBold,
		color: "#666"
	},
	createButton: {
		backgroundColor: "#f4f4f4",
		padding: "16@vs",
		borderRadius: "40@vs",
		marginVertical: "16@vs"
	},
	loginTitle: {
		textTransform: "uppercase",
		fontFamily: Font.Family.OpenSansSemiBold,
		color: "#f4f4f4"
	},
	loginButton: {
		borderWidth: 1,
		borderColor: "#f4f4f4",
		padding: "16@vs",
		borderRadius: "40@vs"
	}
});
