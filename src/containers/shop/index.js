import React, { Component } from "react";
import { View, Text, FlatList } from "react-native";
import { styles } from "./styles";
import CustomView from "../../components/custom-view";
import ShoppingItem from "./components/shopping-item";
import { connect } from "react-redux";
import ShopAction from "../../actions/shop";
import { getShopField } from "../../selector/shop";
import Loader from "../../components/loader";
import { Button } from "react-native-elements";
import Snackbar from "react-native-snackbar";
import { Router } from "../../navigation/route";

class index extends Component {
	state = {};
	componentDidMount() {
		this.props.getList(this.props.user.uid);
	}
	renderItem = ({ item }) => (
		<ShoppingItem
			data={item}
			onPress={() =>
				this.props.navigation.navigate(Router.ListDetail, { item })
			}
		/>
	);
	renderListHeader = () => {
		const { data } = this.props;
		return (
			<View style={[styles.row, { marginVertical: 8 }]}>
				<View style={styles.panel}>
					<Text style={styles.totalList}>{data.length}</Text>
					<Text style={styles.panelText}>Shopping List</Text>
				</View>
				<Button
					onPress={() => this.props.navigation.navigate(Router.AddShopList)}
					type={"outline"}
					title={"New List"}
					titleStyle={styles.btnTitle}
					buttonStyle={styles.btnStyle}
					containerStyle={styles.btnContainer}
				/>
			</View>
		);
	};
	render() {
		const { data, isLoading, error } = this.props;
		return (
			<CustomView>
				<FlatList
					data={data}
					extraData={data}
					keyExtractor={item => item.id}
					renderItem={this.renderItem}
					ListHeaderComponent={this.renderListHeader}
					contentContainerStyle={styles.container}
				/>
				<Loader visible={isLoading} />
			</CustomView>
		);
	}
}

const mapStateToProps = state => ({
	isLoading: getShopField(state, "isLoading"),
	error: getShopField(state, "error"),
	data: getShopField(state, "data"),
	user: state.auth.user
});

const mapDispatchToProps = {
	getList: ShopAction.getList
};

export default connect(mapStateToProps, mapDispatchToProps)(index);
