import React, { Component } from "react";
import { View, Text, StatusBar, Image } from "react-native";
import { styles } from "./styles";
import CustomView from "../../components/custom-view";
import { Router } from "../../navigation/route";
import { connect } from "react-redux";
import AuthAction from "../../actions/auth";
import { getUser } from "../../config/storage";
import logo from "../../assets/images/logo.png";
import Snackbar from "react-native-snackbar";

class index extends Component {
	async componentDidMount() {
		console.log("==========splash");
		try {
			const token = await getUser();
			const user = JSON.parse(token);
			if (user !== null) {
				console.log("===============token", user);

				this.props.setUserData(user);
			}
			this.props.navigation.navigate(user ? Router.Main : Router.Auth);
		} catch (error) {
			console.log("get token failed", error);
			Snackbar.show({
				title: "Something went wrong, please try later"
			});
		}
	}
	render() {
		return (
			<CustomView containerStyle={styles.container}>
				<StatusBar hidden={true} />
				{/* <Loader /> */}
				<Image
					source={logo}
					resizeMode={"contain"}
					resizeMethod={"resize"}
					style={styles.logo}
				/>
			</CustomView>
		);
	}
}

const mapStateToProps = state => ({
	user: state.auth.user
});

const mapDispatchToProps = {
	setUserData: AuthAction.setUserData
};

export default connect(mapStateToProps, mapDispatchToProps)(index);
