import { ScaledSheet } from "react-native-size-matters";

export const styles = ScaledSheet.create({
	container: { flex: 1, justifyContent: "center", alignItems: "center" },
	logo: {
		width: "50@ms",
		height: "50@ms"
	}
});
