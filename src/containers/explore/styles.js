import { ScaledSheet } from "react-native-size-matters";
import { Color } from "../../common";
import { StyleSheet } from "react-native";

export const styles = ScaledSheet.create({
	container: {
		flex: 1,
		backgroundColor: Color.lightGrey
	},
	main: {
		paddingTop: 80
	},
	gradient: {
		...StyleSheet.absoluteFillObject
	},
	slider: {
		overflow: "visible" // for custom animations
	},
	sliderContentContainer: {
		paddingVertical: "8@vs" // for custom animation
	},
	footer: {
		justifyContent: "center",
		alignItems: "center",
		width: '80%',
		height: '80%'
	}
});
