import {
	createAppContainer,
	createSwitchNavigator,
	createStackNavigator,
	createMaterialTopTabNavigator
} from "react-navigation";
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import React from "react";
import Containers from "../containers";
import { Router } from "./route";
import { Color, Font } from "../common";
import { Icon } from "react-native-elements";

const {
	Test,

	Splash,
	Intro,
	Explore,
	Forgot,
	Login,
	Plan,
	Profile,
	Register,
	Shop,
	Search,
	CreateCollection,
	RecipeDetails,
	AddShopList,
	AddToShop,
	ListDetail,
	CookDetail
} = Containers;

const Auth = createStackNavigator(
	{
		[Router.Intro]: Intro,
		[Router.Login]: Login,
		[Router.Register]: Register,
		[Router.Forgot]: Forgot
	},
	{
		// initialRouteName: Router.Register,
		navigationOptions: {
			header: null
		},
		defaultNavigationOptions: {
			header: null
		}
	}
);

const Nav = createMaterialBottomTabNavigator(
	{
		[Router.Explore]: {
			screen: Explore,
			navigationOptions: () => ({
				tabBarIcon: ({ tintColor }) => (
					<Icon name={"home"} type={"antdesign"} size={24} color={tintColor} />
				)
			})
		},
		[Router.Plan]: {
			screen: Plan,
			navigationOptions: () => ({
				tabBarIcon: ({ tintColor }) => (
					<Icon name={"bars"} type={"antdesign"} size={24} color={tintColor} />
				)
			})
		},
		[Router.Shop]: {
			screen: Shop,
			navigationOptions: () => ({
				tabBarIcon: ({ tintColor }) => (
					<Icon
						name={"shoppingcart"}
						type={"antdesign"}
						size={24}
						color={tintColor}
					/>
				)
			})
		},
		[Router.Profile]: {
			screen: Profile,
			navigationOptions: () => ({
				tabBarIcon: ({ tintColor }) => (
					<Icon name={"user"} type={"antdesign"} size={24} color={tintColor} />
				)
			})
		}
	},
	{
		// initialRouteName: Router.Profile,
		navigationOptions: {
			header: null
		},
		activeColor: Color.green,
		inactiveColor: "#ccc",
		barStyle: {
			backgroundColor: Color.lightGrey
		}
	}
);

const Main = createStackNavigator(
	{
		[Router.Nav]: Nav,
		[Router.Test]: Test,
		[Router.Search]: Search,
		[Router.CreateCollection]: CreateCollection,
		[Router.RecipeDetails]: RecipeDetails,
		[Router.AddToShop]: AddToShop,
		[Router.AddShopList]: AddShopList,
		[Router.ListDetail]: ListDetail,
		[Router.CookDetail]: CookDetail
	},
	{
		// initialRouteName: Router.Test,
		navigationOptions: {
			header: null
		}
	}
);

const Switch = createSwitchNavigator(
	{
		[Router.Splash]: Splash,
		[Router.Auth]: Auth,
		[Router.Main]: Main
	},
	{
		// initialRouteName: Router.Auth
	}
);

const AppNav = createAppContainer(Switch);

export default AppNav;
