import { createAction, convertDataToArray } from "../common/util";
import firebase from "react-native-firebase";

export const Types = {
	changeFormField: "change-form-field",
	resetForm: "reset-form",

	getListRequest: "set-list-request",
	getListSuccess: "set-list-success",
	getListFailure: "set-list-failure",

	actionRequest: "action-request",
	actionSuccess: "action-success",
	actionFailure: "action-failure",
};

const changeFormField = (key, value) => dispatch => {
	console.log("change form", key, value);
	dispatch(createAction(Types.changeFormField, { key, value }));
};

const resetForm = () => dispatch => {
	console.log("reset form");
	dispatch(createAction(Types.resetForm));
};

const pushList = (uid, { name, note, ingredients }) => dispatch => {
	console.log("action item", { name, note, ingredients });
	dispatch(createAction(Types.actionRequest));
	firebase
		.database()
		.ref("lists/")
		.push({
			name,
			note,
			ingredients,
			ref: uid,
			createdAt: new Date()
		})
		.then(res => {
			console.log("action item success", res);
			dispatch(createAction(Types.actionSuccess));
		})
		.catch(error => {
			console.log("action item failure", error);
			dispatch(createAction(Types.actionFailure, { error }));
		});
};

const updateList = ({ name, note, ingredients, id }) => dispatch => {
	console.log("update item", { name, note, ingredients });
	dispatch(createAction(Types.actionRequest));
	firebase
		.database()
		.ref(`lists/${id}`)
		.update({
			name,
			note,
			ingredients
		})
		.then(res => {
			console.log("update item success", res);
			dispatch(createAction(Types.actionSuccess));
		})
		.catch(error => {
			console.log("update item failure", error);
			dispatch(createAction(Types.actionFailure, { error }));
		});
};

const removeList = ({ id }) => dispatch => {
	console.log("remove item", { id });
	dispatch(createAction(Types.actionRequest));
	firebase
		.database()
		.ref(`lists/${id}`)
		.remove()
		.then(res => {
			console.log("remove item success", res);
			dispatch(createAction(Types.actionSuccess));
		})
		.catch(error => {
			console.log("remove item failure", error);
			dispatch(createAction(Types.actionFailure, { error }));
		});
};

const getList = uid => dispatch => {
	console.log("get list");
	dispatch(createAction(Types.getListRequest));
	firebase
		.database()
		.ref("lists/")
		.orderByChild("ref")
		.equalTo(uid)
		.once("value")
		.then(res => {
			let data = [];
			if (res.val() !== null) {
				data = convertDataToArray(res.val());
			}

			console.log("get list success", data);
			dispatch(createAction(Types.getListSuccess, { data }));
		})
		.catch(error => {
			console.log("get list failure", error);
			dispatch(createAction(Types.getListFailure, { error }));
		});
};

export default {
	changeFormField,
	resetForm,
	getList,
	pushList,
	updateList,
	removeList
};
