import { createAction } from "../common/util";
import api from "../config/api";

export const Types = {
	getCategoriesRequest: "get-categories-request",
	getCategoriesSuccess: "get-categories-success",
	getCategoriesFailure: "get-categories-failure"
};

const getCategories = () => dispatch => {
	console.log("get categories request");

	dispatch(createAction(Types.getCategoriesRequest));
	api
		.getCategories()
		.then(response => {
			console.log("get categories success");

			dispatch(createAction(Types.getCategoriesSuccess, response));
		})
		.catch(error => {
			console.log("get categories failure");

			dispatch(createAction(Types.getCategoriesFailure, { error }));
		});
};

export default {
	getCategories
};
