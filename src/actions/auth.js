import firebase from "react-native-firebase";
import { createAction } from "../common/util";
import { validateEmail, validatePassword } from "../common/util";
import { setUser } from "../config/storage";

export const Types = {
	resetForm: "reset-form",

	setUserData: "set-user-data",

	getUserRequest: "get-user-request",
	getUserSuccess: "get-user-success",
	getUserFailure: "get-user-failure",

	pushUserRequest: "push-user-request",
	pushUserSuccess: "push-user-success",
	pushUserFailure: "push-user-failure",

	loginRequest: "login-request",
	loginSuccess: "login-success",
	loginFailure: "login-failure",

	logoutRequest: "logout-request",
	logoutSuccess: "logout-success",
	logoutFailure: "logout-failure",

	registerRequest: "register-request",
	registerSuccess: "register-success",
	registerFailure: "register-failure",

	changeFormField: "change-form-field",
	changeField: "change-field",
	toggleCheck: "toggle-check-terms",

	checkEmail: "check-email",
	checkPassword: "check-password",
	checkConfirm: "check-confirm",
	checkUsername: "check-username"
};

const changeFormField = (key, value) => dispatch => {
	console.log("change form", key, value);
	dispatch(createAction(Types.changeFormField, { key, value }));
};

const changeField = (key, value) => dispatch => {
	console.log("change field", key, value);
	dispatch(createAction(Types.changeField, { key, value }));
};

const resetForm = () => dispatch => {
	console.log("reset form");
	dispatch(createAction(Types.resetForm));
};

const setUserData = user => dispatch => {
	console.log("set user data (from async storage)");
	dispatch(createAction(Types.setUserData, { user }));
};

const getUser = uid => dispatch => {
	console.log("get user");
	dispatch(createAction(Types.getUserRequest));
	firebase
		.database()
		.ref(`users/${uid}`)
		.once("value")
		.then(res => {
			const user = res.val();
			console.log("get user success", user);
			dispatch(createAction(Types.getUserSuccess, { user }));
		})
		.catch(error => {
			console.log("get user failure", error);
			dispatch(createAction(Types.getUserFailure, { error }));
		});
};

const pushUser = user => dispatch => {
	const dataUser = Object.assign({}, user);
	dataUser.createAt = new Date();
	console.log("action item", user);
	dispatch(createAction(Types.pushUserRequest));
	firebase
		.database()
		.ref(`users/${user.uid}`)
		.set(dataUser)
		.then(res => {
			console.log("action item success", res);
			dispatch(createAction(Types.pushUserSuccess, { dataUser }));
		})
		.catch(error => {
			console.log("action item failure", error);
			dispatch(createAction(Types.pushUserFailure, { error }));
		});
};

const updateUser = (user, extra = {}) => dispatch => {
	const dataUser = Object.assign(user, extra);
	console.log("update item", { ...user, ...extra });
	dispatch(createAction(Types.pushUserRequest));
	firebase
		.database()
		.ref(`users/${user.uid}`)
		.update(extra)
		.then(res => {
			console.log("update item success", res);
			dispatch(createAction(Types.pushUserSuccess, { dataUser }));
			setUser(dataUser);
		})
		.catch(error => {
			console.log("update item failure", error);
			dispatch(createAction(Types.pushUserFailure, { error }));
		});
};

const logout = () => dispatch => {
	console.log("logout request");
	dispatch(createAction(Types.logoutRequest));
	firebase
		.auth()
		.signOut()
		.then(() => {
			console.log("logout success");
			dispatch(createAction(Types.logoutSuccess));
		})
		.catch(error => {
			console.log("logout failure", error);
			dispatch(createAction(Types.logoutFailure));
		});
};

const login = (email, password) => async dispatch => {
	console.log("login request");

	dispatch(createAction(Types.loginRequest));
	try {
		const res = await firebase
			.auth()
			.signInWithEmailAndPassword(email, password);
		const user = res.user.toJSON();
		console.log("login success", user);
		dispatch(createAction(Types.loginSuccess));
		dispatch(getUser(user.uid));
		dispatch(resetForm());
	} catch (error) {
		console.log("login failure", error, error.code);

		dispatch(createAction(Types.loginFailure, { error: error.code }));
	}
};

const register = (
	email,
	password,
	username = "test user"
) => async dispatch => {
	console.log("register request");
	dispatch(createAction(Types.registerRequest));
	try {
		const res = await firebase
			.auth()
			.createUserWithEmailAndPassword(email, password);
		const user = res.user.toJSON();
		console.log("register success", user);
		const userData = {
			uid: user.uid,
			email: user.email,
			phoneNumber: user.phoneNumber,
			photoURL: user.photoURL,
			displayName: username
		};
		dispatch(createAction(Types.registerSuccess));
		dispatch(pushUser(userData));
		dispatch(resetForm());
	} catch (error) {
		console.log("register failure", error.code);
		dispatch(createAction(Types.registerFailure, { error: error.code }));
	}
};

const toggleCheck = () => dispatch => {
	console.log("toggleCheck");

	dispatch(createAction(Types.toggleCheck));
};

const checkEmail = email => dispatch => {
	console.log("validate email");

	if (validateEmail(email)) {
		dispatch(createAction(Types.checkEmail, { error: "" }));
	} else {
		dispatch(createAction(Types.checkEmail, { error: "Invalid email" }));
	}
};

const checkPassword = password => dispatch => {
	console.log("validate password");

	if (validatePassword(password)) {
		dispatch(createAction(Types.checkPassword, { error: "" }));
	} else {
		dispatch(
			createAction(Types.checkPassword, {
				error: "Password has at lest 6 characters"
			})
		);
	}
};

const checkConfirm = (password, confirm) => dispatch => {
	console.log("validate confirm");

	if (password === confirm) {
		dispatch(createAction(Types.checkConfirm), { error: "" });
	} else {
		dispatch(
			createAction(Types.checkConfirm, { error: "Not similar to password" })
		);
	}
};

const checkUsername = username => dispatch => {
	console.log("validate username");

	if (username !== "") {
		dispatch(createAction(Types.checkUsername, { error: "" }));
	} else {
		dispatch(createAction(Types.checkUsername, { error: "Invalid username" }));
	}
};

export default {
	changeFormField,
	login,
	register,
	toggleCheck,
	checkEmail,
	checkPassword,
	checkConfirm,
	checkUsername,
	getUser,
	updateUser,
	logout,
	setUserData,
	changeField
};
