import { createAction } from "../common/util";
import firebase from "react-native-firebase";
import AuthAction from "./auth";

export const Types = {
	getRecipeRequest: "get-recipe-request,",
	getRecipeSuccess: "get-recipe-success,",
	getRecipeFailure: "get-recipe-failure,",

	setUserLike: "set-user-like",
	toggleUserLike: "toggle-user-like",
	changeCategory: "change-category"
};

const setUserLike = (item, status) => (dispatch, getState) => {
	const {
		auth: { user },
		recipe: { data }
	} = getState();
	const recipeData = data.filter(e => e.name === item.name)[0];
	const index = data.findIndex(e => e.name === item.name);

	console.log("================user.cookBook", user.cookBook);

	let cookBook =
		user.cookBook !== null && user.cookBook !== undefined
			? [...user.cookBook]
			: [];
	if (status) {
		cookBook.push(recipeData);
	} else {
		cookBook.splice(
			cookBook.findIndex(e => e.name === item.name),
			1
		);
	}
	console.log("user like recipes");
	return firebase
		.database()
		.ref(`recipes/${item.uuid}/${user.uid}`)
		.set(status, function(error) {
			if (!error) {
				dispatch(
					createAction(Types.toggleUserLike, {
						status,
						uid: user.uid,
						index
					})
				);
				dispatch(AuthAction.updateUser(user, { cookBook }));
			}
			return new Promise(function(resolve, reject) {
				if (error) {
					reject(error);
				} else {
					resolve(null);
				}
			});
		});
};

// const getRecipes = (category, id = "", reset = false) => dispatch => {
// 	console.log("get recipe request");

// 	dispatch(createAction(Types.getRecipeRequest));
// 	firebase
// 		.database()
// 		.ref("recipes/")
// 		.orderByChild(category)
// 		.startAt(true, id)
// 		.limitToFirst(10)
// 		.once("value")
// 		.then(response => {
// 			console.log(
// 				"get recipe success",
// 				convertDataToArray(response.val()).length
// 			);

// 			dispatch(
// 				createAction(Types.getRecipeSuccess, {
// 					data: convertDataToArray(response.val()),
// 					reset
// 				})
// 			);
// 		})
// 		.catch(error => {
// 			console.log("get recipe failure", error);

// 			dispatch(createAction(Types.getRecipeFailure, { error }));
// 		});
// };

const changeCategory = category => dispatch => {
	console.log("change category");
	dispatch(createAction(Types.changeCategory, { category }));
};

const getRecipes = (id = "") => dispatch => {
	console.log("get recipe request");

	dispatch(createAction(Types.getRecipeRequest));
	firebase
		.database()
		.ref("recipes/")
		.orderByValue()
		.startAt(true, id)
		.limitToFirst(100)
		.once("value")
		.then(response => {
			console.log("get recipe success");

			dispatch(
				createAction(Types.getRecipeSuccess, {
					data: Object.values(response.val())
				})
			);
		})
		.catch(error => {
			console.log("get recipe failure", error);

			dispatch(createAction(Types.getRecipeFailure, { error }));
		});
};

export default {
	getRecipes,
	setUserLike,
	changeCategory
};
