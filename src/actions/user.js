import firebase from "react-native-firebase";
import { createAction } from "../common/util";
import AuthAction from "./auth";

export const Types = {
	uploadAvatar: "upload-avatar"
};

const uploadAvatar = (user, path) => dispatch => {
	return firebase
		.storage()
		.ref(`photos/user/avatar/${user.uid}`)
		.putFile(path)
		.then(file => {
			dispatch(AuthAction.updateUser(user, { photoURL: file.downloadURL }));
			return new Promise(function(resolve) {
				resolve(file.state);
			});
		})
		.catch(error => {
			console.error("upload avatar failed", error);
		});
};

export default {
	uploadAvatar
};
