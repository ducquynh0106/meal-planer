const endpoint = "https://www.themealdb.com/api/json/v1/1/";

class Api {
	constructor(host) {
		this.host = host;
	}

	getRecipes = () => {
		const api = this.host + "search.php?f=b";
		return this.fetchData(api);
	};

	getCategories = () => {
		const api = this.host + "categories.php";
		return this.fetchData(api);
	};

	fetchData = async (api, method = "GET", body) => {
		if (!api.includes(this.host)) {
			api = this.host + api;
		}
		console.log("Method: ", method, "Api", api);
		console.log("Body", body);
		try {
			const options = {
				method,
				headers: {
					Accept: "application/json"
				},
				body
			};
			if (typeof body === "string") {
				options.headers["Content-Type"] = "application/json";
			} else if (body instanceof FormData) {
				options.headers["Content-Type"] = "multipart/form-data";
			}

			const response = await fetch(api, options);
			try {
				const json = await response.json();
				// console.log("Response of", api, json);
				return { ...json, code: json.statusCode, success: response.ok };
			} catch (err) {
				// console.log("response ", response);
				throw err;
			}
		} catch (err) {
			// console.log("Response error'", err);
			return { code: -1, errors: err, success: false };
		}
	};
}

const api = new Api(endpoint);
export default api;
